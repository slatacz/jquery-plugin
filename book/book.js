

window.Book = {
    
    init: function(frame,store) {

        // creation of basic grid
        frame.append("<ul class='list'></ul><div class='details'></div>");

        var last = '';

        // display list element

        var displayList = function(entry,changed=null){
            if(changed != null){
                data =  _.filter(entry,function(element){
                    return element.id !== changed.id
                });
                data.push(changed);
            }else{
                data= entry;
            }
            
            data.sort(function(a, b){
                if(a.name < b.name) { return -1; }
                if(a.name > b.name) { return 1; }
                return 0;
            });
            localStorage.clear();
            localStorage.setItem('characters', JSON.stringify(data));
            $(".list").empty();
            _.map(data,function(element){
                var $item = $(
                    "<li class='item' id="+element.id+">"
                        +"<h4 class='name'>"+element.name+"</h4>"
                        +"<div class='species'>"+element.species+"</div>"
                    +"</li>"
                );
                $item.on("click", function(){renderElement(element.id)});
                $(".list").append($item);
            });
        }

        var loadCharacters = function(){
            // loading characters from store and displaying them
            store.getCharacters().then(function(data){displayList(data)})
            .catch(function(err){throw(err)});
        }
        // initial display of characters list
        loadCharacters();

        var renderElement = function(id){
            last = id;
            details(id);
        }

        var details = function(id){
            // Loading data from store and displaying it in details section
            store.getCharacterDetails(id).then(function(data){
                if(data.id===last){
                    if(data.picture===''){
                        var $content = $(
                            "<div class='container'>"
                                +"<div class='basic-info full-width'>"
                                    +"<h2 class='name editor'>"+data.name+"</h4>"
                                    +"<div class='species editor'>"+data.species+"</div>"
                                +"</div>"
                                +"<div class='description editor'>"+data.description+"</div>"
                                +"<button title='Edit' class='edit'><i class='fa fa-edit'></i></button>"
                            +"</div>"
                        );
                    }else{
                        var $content = $(
                            "<div class='container'>"
                                +"<img src='"+data.picture+"'/>"
                                +"<div class='basic-info'>"
                                    +"<h2 class='name editor'>"+data.name+"</h4>"
                                    +"<div class='species editor'>"+data.species+"</div>"
                                +"</div>"
                                +"<div class='description editor'>"+data.description+"</div>"
                                +"<button title='Edit' class='edit'><i class='fa fa-edit'></i></button>"
                            +"</div>"
                        );
                    }
                    $(".details").html($content);
                    $(".edit").on("click",function(){edit(data)})
                } 
             }).catch(function(err){throw(err)});
        }

        var edit = function(initialObject){

            // DOM manipulation in edit mode 

            $nameInput = $(
                "<input name='name' title='Name' type='text'></input>"
            )
            $speciesInput = $(
                "<input name='species' title='Specie' type='text'></input>"
            )
            $textarea = $(
                "<textarea title='Description' name='description'></textarea>"
            )
            $buttons = $(
                "<button class='save' title='Save'><i class='fa fa-save'></i></button>"
                +"<button class='cancel' title='Cancel'><i class='fa fa-close'></i></button>"
            )

            $(".details .container .description").html($textarea.val(initialObject.description));
            $(".details .container .name").html($nameInput.val(initialObject.name));
            $(".details .container .species").html($speciesInput.val(initialObject.species));

            $(".details .container .edit").remove();
            $(".details .container").append($buttons);

            // Event handlers

            $(".cancel").on("click",function(){
                details(initialObject.id)
            });

            // rerendering components to UI now waiting for store
            $(".save").on("click",function(){
                var modified = initialObject;
                modified.name= $(".name input").val();
                modified.species= $(".species input").val();
                modified.description= $(".description textarea").val();
                store.updateCharacter(modified).catch(function(err){throw(err)});
                var $content = $(
                    "<div class='container'>"
                        +"<img src='"+modified.picture+"'/>"
                        +"<div class='basic-info'>"
                            +"<h2 class='name editor'>"+modified.name+"</h4>"
                            +"<div class='species editor'>"+modified.species+"</div>"
                        +"</div>"
                        +"<div class='description editor'>"+modified.description+"</div>"
                        +"<button title='Edit' class='edit'><i class='fa fa-edit'></i></button>"
                    +"</div>"
                );
                $(".details").html($content);
                $(".edit").on("click",function(){edit(modified)});
                displayList(JSON.parse(localStorage.getItem('characters')),modified);
            });
                    
        }
    }
  };